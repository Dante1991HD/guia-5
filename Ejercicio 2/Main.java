
public class Main {

	public static void main(String[] args) {
		Estudiante estudiante1 = new Estudiante();
		estudiante1.setNombre("Dante");
		estudiante1.setApellido("Aguirre");
		estudiante1.setID(0);
		estudiante1.setCurso("Programación Avanzada");
		Profesor profesor1 = new Profesor();
		profesor1.setNombre("Fabio");
		profesor1.setApellido("Duran");
		profesor1.setID(1);
		profesor1.setAnioIncorporacion(2014);
		profesor1.setNumeroAnexo(0);
		profesor1.setDepartamento("Bioinformática");
		Administrativo administrativo1 = new Administrativo();
		administrativo1.setNombre("Yasna");
		administrativo1.setApellido("Peña");
		administrativo1.setID(2);
		administrativo1.setAnioIncorporacion(2013);
		administrativo1.setNumeroAnexo(1);
		administrativo1.setSeccion("Secretaría");
		System.out.println(profesor1.getNombre() + " " + profesor1.getApellido() + " " + profesor1.getDepartamento());
		System.out.println(administrativo1.getNombre() + " " + administrativo1.getApellido() + " " + administrativo1.getSeccion());
		System.out.println(estudiante1.getNombre() + " " + estudiante1.getApellido() + " " + estudiante1.getCurso());

	}

}
