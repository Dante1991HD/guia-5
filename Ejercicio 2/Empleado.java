
public class Empleado extends Persona{
	
	private int anioIncorporacion;
	private int numeroAnexo;
	public int getAnioIncorporacion() {
		return anioIncorporacion;
	}
	public void setAnioIncorporacion(int anioIncorporacion) {
		this.anioIncorporacion = anioIncorporacion;
	}
	public int getNumeroAnexo() {
		return numeroAnexo;
	}
	public void setNumeroAnexo(int numeroAnexo) {
		this.numeroAnexo = numeroAnexo;
	}
}
