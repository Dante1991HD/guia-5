import java.time.LocalDateTime;

interface Controlador {
	public void prestar(LocalDateTime fechaPrestamo);
	public void devolver();
	public void muestraDetalle();

}
