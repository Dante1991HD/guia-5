import java.time.*;
import java.util.HashMap;

public class Revistas implements Controlador {
	private int codigo;
	private String nombre;
	private int fecha;
	private int edicion;
	private String genero;
	private HashMap <String, Integer> fechaLimite = new HashMap<String, Integer> ();
	private boolean prestado = false;
	private LocalDateTime fechaPrestamo;
	
	Revistas(int codigo, String nombre, int fecha, int edicion, String genero){
		this.setCodigo(codigo);
		this.setNombre(nombre);
		this.setFecha(fecha);
		this.setEdicion(edicion);
		this.setGenero(genero);
		this.fechaLimite.put("Comic", 1);
		this.fechaLimite.put("Cientifica", 2);
		this.fechaLimite.put("Deportiva", 2);
		this.fechaLimite.put("Gastronomica", 3);
	}
	
	public void prestar(LocalDateTime fechaPrestamo) {
		if(!this.prestado){
			setPrestado(true);
			this.fechaPrestamo = fechaPrestamo;
		}else{
			System.out.print(this.getNombre() + ", " + this.getFecha());
			System.out.println(", ISBN: "+ this.getCodigo() + " - " + "no se encuentra disponible");
		}
	}
	
	public void devolver() {
		if(this.prestado){
			System.out.println((retorno() > this.fechaLimite.get(genero)) ? "Tiene " + retorno() +  " días de mora $"  + retorno() * 1290 + " pesos a pagar" : "Se entrega al dia");
			setPrestado(false);
		}
	}
	
	public boolean isPrestado() {
		return prestado;
	}
	public int retorno() {
		int retraso = LocalDateTime.now().compareTo(this.fechaPrestamo);
		return retraso;
	}
	public void setPrestado(boolean prestado) {
			this.prestado = prestado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getFecha() {
		return fecha;
	}

	public void setFecha(int fecha) {
		this.fecha = fecha;
	}

	public int getEdicion() {
		return edicion;
	}

	public void setEdicion(int edicion) {
		this.edicion = edicion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	@Override
	public void muestraDetalle() {
		System.out.print(this.getNombre() + ", " + this.getFecha());
		System.out.println(", ISBN: "+ this.getCodigo() + " - " + (this.prestado ? "Prestado" : "Disponible"));
	}
	
}
