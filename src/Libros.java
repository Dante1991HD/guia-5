fechaLimiteimport java.time.*;

public class Libros implements Controlador  {
	private int codigo;
	private String nombre;
	private int fecha;
	private boolean prestado = false;
	private int fechaLimite = 5;
	private LocalDateTime fechaPrestamo;
	
	Libros(int codigo, String nombre, int fecha){
		this.codigo = codigo;
		this.nombre = nombre;
		this.fecha = fecha;
	}
	
	public void prestar(LocalDateTime fechaPrestamo) {
		if(!this.prestado){
			setPrestado(true);
			this.fechaPrestamo = fechaPrestamo;
		}else{
			System.out.print(this.getNombre() + ", " + this.getFecha());
			System.out.println(", ISBN: "+ this.getCodigo() + " - " + "no se encuentra disponible");
		}
	}
	
	public void devolver() {
		if(this.prestado){
			System.out.println((retorno() > fechaLimite) ? "Tiene " + retorno() +  " días de mora $"  + retorno() * 1290 + " pesos a pagar" : "Se entrega al dia");
			setPrestado(false);
		}
	}
	public int retorno() {
		int retraso = LocalDateTime.now().compareTo(this.fechaPrestamo);
		return retraso;
	}
	public boolean isPrestado() {
		return prestado;
	}
	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getFecha() {
		return fecha;
	}
	public void setFecha(int fecha) {
		this.fecha = fecha;
	}

	public LocalDateTime getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDateTime fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	@Override
	public void muestraDetalle() {
		System.out.print(this.getNombre() + ", " + this.getFecha());
		System.out.println(", ISBN: "+ this.getCodigo() + " - " + (this.prestado ? "Prestado" : "Disponible"));

	}
}
