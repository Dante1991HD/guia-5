import java.util.ArrayList;

public class Biblioteca {
    private ArrayList < Libros > libro = new ArrayList < Libros > ();
    private ArrayList < Revistas > revista = new ArrayList < Revistas > ();
    
	public ArrayList < Libros > getLibro() {
		return libro;
	}
	public void setLibro(Libros libro) {
		this.libro.add(libro);
	}
	public ArrayList < Revistas > getRevista() {
		return revista;
	}
	public void setRevista(Revistas revista) {
		this.revista.add(revista);
	}
}
