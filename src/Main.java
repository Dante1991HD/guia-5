import java.time.*;
public class Main {

	public static void main(String[] args) {
		Biblioteca biblioteca = new Biblioteca();
		Libros libro1 = new Libros(42069, "El Señor de los Aliños - Ozcar la Capital", 2021);
		biblioteca.setLibro(libro1);
		Libros libro2 = new Libros(69420, "Juan Pizz - Eichiro Oda-sensei", 1900);
		biblioteca.setLibro(libro2);
		Revistas revista1 = new Revistas(666, "Shingeki no Kyojin vol 9 - Hajime Isayama-sensei", 2009, 2, "Comic");
		biblioteca.setRevista(revista1);
		Revistas revista2 = new Revistas(667, "Shingeki no Kyojin vol 7 - Hajime Isayama-sensei", 2009, 2, "Cientifica");
		biblioteca.setRevista(revista2);
		for(Libros l: biblioteca.getLibro()) {
			System.out.println(l.getNombre());
		}
		for(Revistas r: biblioteca.getRevista()) {
			System.out.println(r.getNombre());
		}
		libro1.muestraDetalle();
		revista2.muestraDetalle();
		LocalDateTime fechaPrestamo = LocalDateTime.of(2021, 05, 25, 22, 49, 15);
		revista1.prestar(fechaPrestamo);
		revista1.devolver();
		fechaPrestamo = LocalDateTime.of(2021, 05, 24, 22, 49, 15);
		revista1.prestar(fechaPrestamo);
		revista1.devolver();
		fechaPrestamo = LocalDateTime.of(2021, 05, 26, 22, 49, 15);
		revista2.prestar(fechaPrestamo);
		revista2.devolver();
		fechaPrestamo = LocalDateTime.of(2021, 05, 11, 22, 49, 15);
		revista2.prestar(fechaPrestamo);
		revista2.devolver();
	}

}
